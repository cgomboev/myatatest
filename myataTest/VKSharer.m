//
//  VKSharer.m
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "VKSharer.h"
#import <VKSdk.h>

#define kVKAppId @"3067571"
@interface VKSharer () <VKSdkDelegate>
{
    NSString* _shareText;
    NSString* _attachments;
    
}
@property (nonatomic,copy) CompletionHandler complete;
@end

@implementation VKSharer

+ (instancetype) shared
{
    static VKSharer *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[VKSharer alloc] init];
    });
    
    return _sharedClient;
}

-(void)shareToVK:(NSString* )text attachments:(NSString* )attachment completion:(CompletionHandler) complete
{
    self.complete = complete;
    _shareText = text;
    _attachments = attachment;
    
    [VKSdk initializeWithDelegate:self andAppId:kVKAppId];
    if ([VKSdk wakeUpSession] && [VKSdk hasPermissions:@[@"wall"]])
    {
        [self shareGranted];
    }
    else
    {
        _shareText = [text copy];
        [VKSdk authorize:@[@"offline",@"wall"] revokeAccess:YES];
    }
}

-(void)shareGranted
{
    NSMutableDictionary* params = [@{VK_API_MESSAGE : _shareText,
                                 VK_API_ATTACHMENTS : _attachments} mutableCopy];
    
    VKRequest * postReq = [[VKApi wall] post:params];
    [postReq executeWithResultBlock:^(VKResponse *response) {
        
        if(self.complete)
            self.complete(YES,nil);
        
    } errorBlock:^(NSError *error) {
        
        if(self.complete)
            self.complete(NO,error);
        
    }];
}



#pragma OFICIAL VK SDK DELEGATE METHODS
//required
-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    
}

-(void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    
}

-(void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    if(self.complete)
        self.complete(NO,authorizationError);
}

-(void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [[[[UIApplication sharedApplication] windows].firstObject rootViewController] presentViewController:controller animated:YES completion:^{
        
    }];
}

-(void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    
    [self shareGranted];
}

//optional

-(void)vkSdkRenewedToken:(VKAccessToken *)newToken
{
    [self vkSdkReceivedNewToken:newToken];
}



@end
