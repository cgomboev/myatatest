//
//  GameController.h
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameController : UIViewController

@property (nonatomic, copy) NSString* quoteText;
@property (weak, nonatomic) IBOutlet UILabel *quoteLabel;

@property (weak, nonatomic) IBOutlet UITextField *invisibleTextField;
@end
