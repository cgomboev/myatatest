//
//  GameController.m
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "GameController.h"
#import "ScoresController.h"

@interface GameController () <UITextFieldDelegate>
{
    NSRange _currentCharRange;
    NSArray* _words;
    NSArray* _wordsRanges;
    NSInteger _currentWordIndex;
    NSDate* _levelBeginDate;
    NSDate* _levelEndDate;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quoteLeadingContraint;


@end

@implementation GameController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _words = [self.quoteText componentsSeparatedByString:@" "];
    
    NSMutableArray *tempWords = _words.mutableCopy;
    NSMutableArray *tempRanges = [@[] mutableCopy];
    __block NSString* tempQuoteText = self.quoteText.copy;
    __block NSInteger lengthOfDeletedWords = 0;
    
    //все это проделываю чтобы избежать случая когда в тексте есть одинаковые слова
    [tempWords enumerateObjectsUsingBlock:^(NSString* word, NSUInteger idx, BOOL *stop) {
       
        NSRange range = [tempQuoteText rangeOfString:word];
        range.location += lengthOfDeletedWords;
        [tempRanges addObject: NSStringFromRange(range)];
        lengthOfDeletedWords = range.location + range.length;
        tempQuoteText = [self.quoteText substringFromIndex:(range.location + range.length)];

    }];
    _wordsRanges = tempRanges.copy;
    [self.navigationItem setHidesBackButton:YES animated:NO];

    _currentCharRange = NSMakeRange(0, 1);
    
    self.quoteLabel.text = self.quoteText;
    [self.quoteLabel layoutIfNeeded];
    [self layoutQuoteLabel];
    
    _levelBeginDate = [NSDate date];
    [ScoresManager shared].numberOfError = 0;
    
    // Do any additional setup after loading the view.
}

-(void)layoutQuoteLabel
{
    CGFloat halfScreen = CGRectGetWidth([UIScreen mainScreen].bounds)/2;
    
    NSDictionary* dict = @{
                           NSFontAttributeName : [UIFont systemFontOfSize:30],
                           NSForegroundColorAttributeName : [UIColor blackColor],
                           NSBackgroundColorAttributeName : [UIColor whiteColor]
                           };
    
    NSString* prevText = [self.quoteText substringToIndex:_currentCharRange.location];
    CGRect prevTextRect = [prevText boundingRectWithSize:CGSizeMake(MAXFLOAT,21) options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:_quoteText attributes:dict];
    [attrString addAttributes:@{
                               NSBackgroundColorAttributeName : [UIColor greenColor],
                               NSFontAttributeName : [UIFont systemFontOfSize:35]
                               } range:NSRangeFromString(_wordsRanges[_currentWordIndex])];
    [attrString addAttributes:@{
                               NSBackgroundColorAttributeName : [UIColor yellowColor],
                               NSFontAttributeName : [UIFont boldSystemFontOfSize:40]
                               } range:_currentCharRange];
    [self.quoteLabel setAttributedText:attrString];
    self.quoteLeadingContraint.constant = halfScreen - CGRectGetWidth(prevTextRect);
    [self.quoteLabel layoutIfNeeded];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_invisibleTextField becomeFirstResponder];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([[string lowercaseString] isEqualToString:[[self.quoteText substringWithRange:_currentCharRange] lowercaseString] ])
    {
        [self nextCharacter];
    }
    else
    {
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        BOOL hasBackspace = [newString length] < [textField.text length];
        if(!hasBackspace)
        {
            [ScoresManager shared].numberOfError ++;
        }
        
    }
    return NO;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return NO;
}


-(void)nextCharacter
{
    _currentCharRange.location++;

    NSRange currentWordRange = [self.quoteText rangeOfString: _words[_currentWordIndex] ];
    if(_currentCharRange.location > (currentWordRange.location+currentWordRange.length))
        _currentWordIndex++;
        
    if(_currentCharRange.location==(self.quoteText.length))
        [self finish];
    else
        [self layoutQuoteLabel];
}

-(void)finish
{
    _levelEndDate = [NSDate date];
    [ScoresManager shared].levelDuration = ABS([_levelEndDate timeIntervalSinceDate:_levelBeginDate]);
    ScoresController *controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ScoresController"];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
