//
//  CGSessionMananer.m
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "CGSessionMananer.h"
#import <AFNetworking.h>

@interface CGSessionMananer ()
@end

@implementation CGSessionMananer

static NSString * const AFCGAPIBaseURLString = @"http://api.forismatic.com/api/1.0/";
static NSString * const AFCGAPIBaseURLString2 = @"https://andruxnet-random-famous-quotes.p.mashape.com";

+ (instancetype) shared
{
    static CGSessionMananer *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[CGSessionMananer alloc] init];
    });
    
    return _sharedClient;
    
}

-(void)getRandomQuoteWithSuccess:(SuccessHandler) success failure:(FailureHandler) failure
{
    //используем nsurlconnection потому что iOS6 еще поддерживать. не стал тут разбивать на случаи
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?method=getQuote&format=json&lang=en",AFCGAPIBaseURLString ]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(!responseObject)
        {
            [self getRandomQuoteWithSuccess:success failure:failure];
            return;
        }
        
        
        if(success)
            success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
            failure(error);
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
}


///запасной метод другого апи
-(void)getRandomQuote2WithSuccess:(SuccessHandler) success failure:(FailureHandler) failure
{
    //используем nsurlconnection потому что iOS6 еще поддерживать. не стал тут разбивать на случаи
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/cat=movies",AFCGAPIBaseURLString2 ]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    [request setValue:@"2wYaQpVnEomshavKpRvBEyWSH0MPp1iagccjsnfY8TCxBkNoJ6" forHTTPHeaderField:@"X-Mashape-Key"];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    AFJSONResponseSerializer* serializer = [AFJSONResponseSerializer serializer];
    serializer.acceptableContentTypes = [NSSet setWithObjects: @"application/x-www-form-urlencoded", @"text/html",nil];
    op.responseSerializer = serializer;
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
    
        if(success)
            success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
            failure(error);
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
}


-(void )getURLShortener:(NSString* ) shortURL success:(SuccessHandler) success failure:(FailureHandler) failure
{
    
    NSString *apiEndpoint = [NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@",shortURL?:@"https://google.com"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:apiEndpoint]];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString* str = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];
        
        if(success)
            success(str);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(failure)
            failure(error);
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
}


@end
