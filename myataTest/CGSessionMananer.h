//
//  CGSessionMananer.h
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "AFURLSessionManager.h"

typedef void (^SuccessHandler)(id responseObject);
typedef void (^FailureHandler)(NSError *error);

@interface CGSessionMananer : NSObject 

@property (retain, readonly) NSString *userEmail;

+ (instancetype) shared;

-(void)getRandomQuoteWithSuccess:(SuccessHandler) success failure:(FailureHandler) failure;

///запасной метод другого апи, а то первый отваливается время от времени
-(void)getRandomQuote2WithSuccess:(SuccessHandler) success failure:(FailureHandler) failure;

-(void )getURLShortener:(NSString* ) shortURL success:(SuccessHandler) success failure:(FailureHandler) failure;

@end
