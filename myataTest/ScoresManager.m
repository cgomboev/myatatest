//
//  ScoresManager.m
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "ScoresManager.h"

@implementation ScoresManager

+ (instancetype) shared
{
    static ScoresManager *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[ScoresManager alloc] init];
    });
    
    return _sharedClient;
}

@end
