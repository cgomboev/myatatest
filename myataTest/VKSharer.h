//
//  VKSharer.h
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompletionHandler)(BOOL shared, NSError *error);

@interface VKSharer : NSObject

+ (instancetype) shared;
-(void)shareToVK:(NSString* )text attachments:(NSString* )attachment completion:(CompletionHandler) complete;

@end
