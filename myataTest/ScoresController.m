//
//  ScoresController.m
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "ScoresController.h"
#import "VKSharer.h"

@interface ScoresController ()

@property (nonatomic,weak) IBOutlet UILabel *label1;
@property (nonatomic,weak) IBOutlet UILabel *label2;
@property (nonatomic,weak) IBOutlet UILabel *label3;

@end

@implementation ScoresController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Quote* quote = [ScoresManager shared].quote;
    
    _label1.text = [NSString stringWithFormat:@"number of characters: %lu", (unsigned long)quote.quoteText.length];
    _label2.text = [NSString stringWithFormat:@"speed %0.1f char/min" , quote.quoteText.length / [ScoresManager shared].levelDuration * 60];
    _label3.text = [NSString stringWithFormat:@"numbers of errors %ld" , (long)[ScoresManager shared].numberOfError];

    [self.navigationItem setHidesBackButton:YES animated:NO];

    
    // Do any additional setup after loading the view.
}

- (IBAction)shareToVk:(id)sender {
    
    Quote* quote = [ScoresManager shared].quote;
    
    [SVProgressHUD show];
    
    __weak typeof(self) weakSelf = self;
    [[VKSharer shared] shareToVK:[NSString stringWithFormat:@"Я набрал текст: %@ за %0.0f секунд, скорость набора: %0.1f симв/мин",quote.shortLink,[ScoresManager shared].levelDuration, quote.quoteText.length / [ScoresManager shared].levelDuration*60] attachments:quote.shortLink completion:^(BOOL shared, NSError *error) {
        
        if(shared)
            [SVProgressHUD showSuccessWithStatus:@"Shared"];
        else if (error)
            [SVProgressHUD showErrorWithStatus:@"Error"];
        
        [weakSelf performSelector:@selector(retry:) withObject:nil afterDelay:2];
    }];
    
}

-(IBAction)retry:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

@end
