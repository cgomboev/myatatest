//
//  ViewController.m
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "ViewController.h"
#import "Quote.h"
#import "GameController.h"

#define kReverseTimeInterval 7
#define kSegueToGameController @"kSegueToGameController"
@interface ViewController ()

@property (nonatomic, strong) Quote* quote;
@property (nonatomic, strong) NSTimer* timer;
@property (nonatomic, copy) NSString* recalculatedQuote;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textView.textColor = [UIColor blackColor];
    
    [self.navigationItem setHidesBackButton:YES animated:NO];

//    self.textView.textContainerInset = UIEdgeInsetsZero;
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self retry:nil];

}

-(IBAction)retry:(id)sender
{
    self.textView.text = nil;
    self.quote = nil;
    self.timerLabel.text = nil;
    
    [SVProgressHUD show];
    
    [Quote getRandomQuote:^(Quote *quote, NSError *error) {
        
        if(!error)
        {
            self.quote = quote;
            
            _recalculatedQuote = [self recalculateText: quote.quoteText];
            
            [SVProgressHUD dismiss];

            [self startInvertedTimer];
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        }
    }];
}

-(NSString *)recalculateText:(NSString *)quoteText //подгоняем примерно под 300 символов
{
    if(quoteText.length < 300)
        return quoteText;
    
    NSArray* sentenses = [quoteText componentsSeparatedByString:@". "];
    quoteText = sentenses.firstObject;
    for(NSString* sentense in sentenses)
    {
        if(![sentense isEqualToString:quoteText])
        {
            quoteText = [@[quoteText,sentense] componentsJoinedByString:@". "];
        }
        if(quoteText.length>=300)
            break;
    }
    
    return quoteText;
}

-(void)startInvertedTimer
{
    if(self.timer)
        [self.timer invalidate];
    
    self.timer = [NSTimer timerWithTimeInterval:1
                                         target:self
                                       selector:@selector(startGame:)
                                       userInfo:[@{@"timeLeft" : @(kReverseTimeInterval)} mutableCopy]
                                        repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

-(void)startGame:(NSTimer *)timer
{
    NSInteger timeLeft = [timer.userInfo[@"timeLeft"] integerValue];
    
    if(timeLeft == 3)
    {
        self.textView.text = _recalculatedQuote;
    }
    
    if(!timeLeft)
    {
        [self.timer invalidate];

        GameController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GameController"];
        vc.quoteText = self.recalculatedQuote;
        [self.navigationController pushViewController:vc animated:NO];
        [ScoresManager shared].quote = self.quote;
        return;
    }
    
    self.timerLabel.text = [@"Start after " stringByAppendingString:[@(timeLeft) stringValue]];
    timeLeft--;
    
    timer.userInfo[@"timeLeft"] = @(timeLeft);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
