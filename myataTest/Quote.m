//
//  Quote.m
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import "Quote.h"

@implementation Quote

+(void)getRandomQuote:(void (^)(Quote* quote, NSError* error))completion
{
    __block Quote* quote = [Quote new];
    
    __weak typeof(self) weakSelf = self;
    [[CGSessionMananer shared] getRandomQuoteWithSuccess:^(NSDictionary *responseObject) {
        
        if(!responseObject) //чтото апи иногда ничего не возращает, просто повторяем попытку
        {
            [weakSelf getRandomQuote:completion];
            return ;
        }
        
        //ну для одного ключ хэш-маппинг неохото заводить
        quote.quoteText = responseObject[@"quoteText"];
        quote.quoteLink = responseObject[@"quoteLink"];
        
        //от запосного апи цитат
        quote.quoteText = responseObject[@"quote"] ? :quote.quoteText;
        
        // там чето последним символом пробел ненужный, удаляю
        quote.quoteText = [quote.quoteText stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        
        //здесь уже ассинхронно запрошиваем
        [[CGSessionMananer shared] getURLShortener:quote.quoteLink success:^(NSString* responseObject) {
            
            quote.shortLink = responseObject;
            
            if(completion)
                completion(quote,nil);
            
        } failure:^(NSError *error) {
            
            if(completion)
                completion(quote,nil);
        }];
        
    } failure:^(NSError *error) {
        
        if(completion)
            completion(nil,error);
    }];
}

- (id)copyWithZone:(NSZone *)zone {
    Quote *quote = [Quote new];
    quote.quoteText = self.quoteText;
    quote.quoteLink = self.quoteLink;
    quote.shortLink = self.shortLink;
    
    return quote;
}

@end
