//
//  Quote.h
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Quote : NSObject

@property (nonatomic, copy) NSString *quoteText;
@property (nonatomic, copy) NSString *quoteLink;
@property (nonatomic, copy) NSString *shortLink;

+(void)getRandomQuote:(void (^)(Quote* quote, NSError* error))completion;

@end
