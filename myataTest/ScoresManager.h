//
//  ScoresManager.h
//  myataTest
//
//  Created by qwerty on 06.12.14.
//  Copyright (c) 2014 chingis_gomboev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quote.h"

@interface ScoresManager : NSObject

+ (instancetype) shared;

@property (nonatomic, copy) Quote *quote;
@property (nonatomic) NSInteger numberOfError;
@property (nonatomic) NSTimeInterval levelDuration;

@end
